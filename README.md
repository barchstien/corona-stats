# Introduction
Corona daily cases, infection, infection variation

# Install
## Anaconda
- Download and install Anaconda https://www.anaconda.com/distribution/
```bash
# use latest link from website
wget https://repo.anaconda.com/archive/Anaconda3-2019.07-Linux-x86_64.sh
chmod u+x Anaconda3-*
./Anaconda3-[...]
```
**Do not install Anaconda in your PATH or .bashrc**

### Setup environment
```bash
# load conda
source /home/bastien/anaconda3/etc/profile.d/conda.sh

# list existing environments
conda env list
# if envirronment does not exist, create it, else ignore next line
conda env create -f corona-stats.yml

# activate env
conda activate corona-stats
# deactivate env
conda deactivate

```

# Notes
Info : https://towardsdatascience.com/getting-started-with-python-environments-using-conda-32e9f2779307
```bash
# create env from scratch
conda create --name corona-stats python=3
# export env
conda env export > environment.yml
# delete env
conda remove --name table-top-map --all

# create from scratch
conda activate corona-stats
conda install bokeh pandas xmltodict
```

# Pandas resample notes
```python
s = pd.Series(france_cases, index=france_dates)
print(s)
print('---')
df = pd.DataFrame({'s':s})
print(df)
print('---')
#print(s.resample('2D').sum())
df2 = df.asfreq(freq='2D')
print(df2)
```


