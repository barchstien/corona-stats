import urllib.request, json
import gzip
import xmltodict

from bokeh.plotting import figure, output_file, show, save
from datetime import datetime
from bokeh.models import DatetimeTickFormatter
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, HoverTool
from bokeh.models import Panel, Tabs, PreText

import pandas as pd

from bokeh.palettes import Category20_20 as palette
import itertools

import time
import sys, os

# Weekly data from 01/01/2020
#  https://data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data-weekly-from-17-december-2020
#  https://opendata.ecdc.europa.eu/covid19/nationalcasedeath/xml

DATA_URL            = 'https://opendata.ecdc.europa.eu/covid19/nationalcasedeath/xml'
OUT_HTML_PATH       = 'public/corona_stats.html'

COUNTRIES = ['FRA', 'DEU', 'GBR', 'USA', 'NOR', 'SWE', 'ITA', 'ESP', 'POL', 'ISR', 'NLD', 'CHL', 'BRA', 'RUS', 'CHN', 'IND']
NOT_MUTED = [   1,     1,     0,     0,     1,     1,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0]

#### Get data
data = dict()
MAX_TRY_URLOPEN = 10
urlopen_cnt = 0
got_data_from_url = False
exception = 0

while not got_data_from_url and urlopen_cnt < MAX_TRY_URLOPEN :
    try:
        with urllib.request.urlopen(DATA_URL, timeout=60) as url:
            print(url.info().get_content_type())
            if url.info().get_content_type() == 'text/xml' :
                xml_data = xmltodict.parse(url.read().decode())
                data['records'] = xml_data['fme:records']['fme:Sheet-table']['fme:Sheet']
            elif url.info().get_content_type() == 'application/json' :
                # not maintained
                data = json.loads(url.read().decode())
            got_data_from_url = True
    except Exception as e:
        print ("Faild to reach URL : ", DATA_URL, "\nRetrying...")
        time.sleep(10)
        urlopen_cnt += 1
        exception = e

if not got_data_from_url :
    print ("Faild to reach URL : ", DATA_URL, "\nGiving up for now...")
    print ("Error was: ", exception)
    sys.exit(51)


country_series = []

colors = itertools.cycle(palette)

def get_float_or_nan(item, key):
    try:
        return float(item[key])
    except:
        return float('nan')

#### Filter data
for code in COUNTRIES :
    try:
        e = dict()
        # Filter only for relevant country
        # Cannot use lambda directly coz there's also data per continent
        ##country_serie = list(filter(lambda x : x['fme:country_code'] == code, data['records']))
        country_serie = []
        for d in data['records'] :
            try:
                if d['fme:country_code'] == code :
                    country_serie.append(d)
            except:
                pass
        
        if len(country_serie) <= 0:
            print("No data for country code : " + code)
            # increment color to keep it consistent accross runs
            next(colors)
            # go to next country
            continue
        
        # Filter only cases
        print("Filtering cases for country code : " + code)
        country_serie_cases = list(filter(lambda x : x['fme:indicator'] == 'cases', country_serie))
        e['weekly_cases'] = list(map(
            lambda x : get_float_or_nan(x, 'fme:weekly_count'), country_serie_cases
        ))
        e['weekly_cases_cumul'] = list(map(
            lambda x : get_float_or_nan(x, 'fme:cumulative_count'), country_serie_cases
        ))
        # Get the date from cases only, else twice more date than cases/deaths
        # Add '-1' after 2020-01 so the algo can get the first day of the week
        # using anything that first day of the week is to avoid coz of the last week of the year
        e['date'] = list(map(
            lambda x : datetime.strptime(x['fme:year_week'] + '-1', "%Y-%W-%w"), country_serie_cases
        ))
        
        # Filter only deaths
        print("Filtering deaths for country code : " + code)
        country_serie_deaths = list(filter(lambda x : x['fme:indicator'] == 'deaths', country_serie))
        e['weekly_deaths'] = list(map(
            lambda x : get_float_or_nan(x, 'fme:weekly_count'), country_serie_deaths
        ))
        e['weekly_deaths_cumul'] = list(map(
            lambda x : get_float_or_nan(x, 'fme:cumulative_count'), country_serie_deaths
        ))
        
        e['name'] = list(map(lambda x : x['fme:country'], country_serie))[0]
        e['population'] = list(map(lambda x : int(x['fme:population']), country_serie))[0]
        e['color'] = next(colors)
        default_muted = True
        try:
            default_muted = NOT_MUTED[COUNTRIES.index(code)] != 1
        except:
            pass
        e['muted'] = default_muted
        country_series.append(e)
    except Exception as e:
        print(sys.exc_info())
        print(str(e))
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        print(country_serie)

#sys.exit(51)

################# Plot

def make_figure(serie_name, date_name, title='', divider_serie_name='', y_multiplier=1):
    f = figure(plot_width=800, plot_height=600, sizing_mode="stretch_width", x_axis_type="datetime",
        title=title, title_location="above")
    f.add_tools(HoverTool(
        tooltips = [
            ("country", "@name"),
            ("date", "@date{%F}"),
            ("y", "@y"),
        ],
        formatters = {
            '@date' : 'datetime',
        },
    ))
    f.toolbar.active_drag = None
    # For each country, get the serie with "serie_name" name
    for country in country_series :
        serie = country[serie_name]
        # If a divider is specified, create a new serie from it
        if len(divider_serie_name) != 0 :
            serie = []
            for e in country[serie_name] :
                serie.append(e / country[divider_serie_name] * y_multiplier)
        source = ColumnDataSource(data=dict(
            date = country[date_name],
            y    = serie,
            name = [country['name']] * len(serie)
        ))
        serie_line = f.line('date', 'y', source=source, line_width=2, line_color=country['color'],
            legend_label=country['name'])
        serie_circle = f.circle('date', 'y', source=source, size=4, color=country['color'],
            legend_label=country['name'])
        # get if serie should be muted by default
        serie_circle.visible = not country['muted']
        serie_line.visible = not country['muted']
    # config legend
    f.legend.location='top_left'
    f.legend.click_policy="hide"
    f.legend.background_fill_alpha = 0.3
    f.xaxis[0].formatter = DatetimeTickFormatter(
        days=['%d/%m/%Y'],
        months=['%d/%m/%Y'],
        hours=['%d/%m/%Y'],
        minutes=['%d/%m/%Y']
    )
    return f

output_file(OUT_HTML_PATH)

#, y_axis_type="log"

## Cases
print("Cases Total...")
#### Total
total_cases = make_figure('weekly_cases_cumul', 'date', 'Total cases')
#### Weekly
print("Cases Weekly...")
weekly_cases = make_figure('weekly_cases', 'date', 'Weekly infection')
#### Growth
#growth_cases = make_figure('growth_cases', 'weekly_date', 'Infection growth')

#### Total % population
total_cases_permillion = make_figure('weekly_cases_cumul', 'date', 'Total case per million', 'population', 1e6)
#### Weekly % population
weekly_cases_permillion = make_figure('weekly_cases', 'date', 'Weekly infection per million', 'population', 1e6)
#### Growth % population
#growth_cases_permillion = make_figure('growth_cases', 'weekly_date', 'Growth infection per million', 'population', 1e6)

tab_total_cases            = Panel(child=total_cases, title='absolute')
tab_total_cases_permillion = Panel(child=total_cases_permillion, title='per million')
tabs_total_cases           = Tabs(tabs=[ tab_total_cases_permillion, tab_total_cases ])

tab_weekly_cases            = Panel(child=weekly_cases, title='absolute')
tab_weekly_cases_permillion = Panel(child=weekly_cases_permillion, title='per million')
tabs_weekly_cases           = Tabs(tabs=[ tab_weekly_cases_permillion, tab_weekly_cases ])



## Deaths
print("Deaths...")
#### Total
total_deaths = make_figure('weekly_deaths_cumul', 'date', 'Total deaths')
#### Daily
daily_deaths = make_figure('weekly_deaths', 'date', 'Weekly deaths')

#### Total % population
total_deaths_permillion = make_figure('weekly_deaths_cumul', 'date', 'Total death per million', 'population', 1e6)
#### Daily % population
daily_deaths_permillion = make_figure('weekly_deaths', 'date', 'Weekly death per million', 'population', 1e6)


tab_total_deaths            = Panel(child=total_deaths, title='absolute')
tab_total_deaths_permillion = Panel(child=total_deaths_permillion, title='per million')
tabs_total_deaths           = Tabs(tabs=[ tab_total_deaths_permillion, tab_total_deaths ])

tab_daily_deaths            = Panel(child=daily_deaths, title='absolute')
tab_daily_deaths_permillion = Panel(child=daily_deaths_permillion, title='per million')
tabs_daily_deaths           = Tabs(tabs=[ tab_daily_deaths_permillion, tab_daily_deaths ])


######### Source
text_source = PreText(text="""
Source : 
  https://data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data-weekly-from-17-december-2020
""" + 'Last updated : ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S") + ' UTC')

# TODO in %, in log
# TODO more countries
# TODO daily variation

# , growth_cases
# , growth_cases_permillion
show(
    column(
        row(
            tabs_total_cases, tabs_weekly_cases
        ),
        row(
            tabs_total_deaths, tabs_daily_deaths
        ), 
        text_source
    )
    )



